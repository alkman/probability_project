# Generated by Django 2.0.6 on 2018-07-17 21:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0003_auto_20180707_1457'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='order',
            field=models.PositiveIntegerField(db_index=True, default=1, editable=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='article',
            name='change_date',
            field=models.DateTimeField(auto_now=True, verbose_name='Остання зміна'),
        ),
        migrations.AlterField(
            model_name='article',
            name='pub_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Дата публікації'),
        ),
        migrations.AlterUniqueTogether(
            name='article',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='article',
            name='sequence_number',
        ),
    ]
