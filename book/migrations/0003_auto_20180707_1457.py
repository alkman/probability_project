# Generated by Django 2.0.6 on 2018-07-07 11:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0002_auto_20180707_1339'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='sequence_number',
            field=models.PositiveIntegerField(blank=True, verbose_name='Порядковий номер статті'),
        ),
        migrations.AlterUniqueTogether(
            name='article',
            unique_together={('topic', 'sequence_number')},
        ),
    ]
