from easy_thumbnails.signals import saved_file
from easy_thumbnails.signal_handlers import generate_aliases_global

from django.db.models.signals import pre_delete

from book.models import ImageAtachment


def delete_thunmbnail_files(sender, instance, using, **kwargs):
    # Видаляємо thumbnails 
    instance.image.delete_thumbnails()
    instance.image.delete(save=False)


saved_file.connect(generate_aliases_global)
pre_delete.connect(delete_thunmbnail_files, sender=ImageAtachment)