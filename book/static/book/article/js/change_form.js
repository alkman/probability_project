var content_textarea = document.getElementsByName('content')[0];

var codeMirror = CodeMirror.fromTextArea(
    content_textarea,
    {
        theme: 'idea',
        lineNumbers: true,
        viewportMargin: 30,
        extraKeys: {
            "F11": function(cm) {
              cm.setOption("fullScreen", !cm.getOption("fullScreen"));
            },
            "Esc": function(cm) {
              if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
            }
        }
    }
);


var converter = new showdown.Converter({
    tables: true,
});

$("#article-preview-modal").on("show.bs.modal", function () {
    $(this).find(".modal-content").html(converter.makeHtml(codeMirror.getValue()));
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, this]);
});

$("input[type=submit]").click(function () {
    codeMirror.save();
})