from django.contrib import admin
from django.utils.html import format_html
from django.template.loader import render_to_string
from django.urls import reverse

from easy_thumbnails.fields import ThumbnailerField
from easy_thumbnails.widgets import ImageClearableFileInput

from ordered_model.admin import OrderedModelAdmin

from .models import Article, Topic, ImageAtachment


class TopicAdmin(OrderedModelAdmin):
    
    list_display = ('name', 'order', 'move_up_down_links')
    ordering = ('order',)

    OrderedModelAdmin.move_up_down_links.short_description = 'Зміна порядку'


class ImageAtachmentInline(admin.TabularInline):
    model = ImageAtachment
    formfield_overrides = {
        ThumbnailerField: {'widget': ImageClearableFileInput},
    }

    fields = ('image', 'image_urls')
    readonly_fields = ('image_urls',)

    def image_urls(self, obj):
        return format_html(
            '<p>Оригінал: {}</p>\n<p>Стиснена: {}</p>',
            obj.image.url,
            obj.image['article_image'].url
        )
    image_urls.short_description = 'Посилання для markdown'


class ArticleAdmin(OrderedModelAdmin):
    inlines = [
        ImageAtachmentInline
    ]

    list_display = ('name', 'change_date', 'pub_date', 'topic', 'order', 'move_up_down_links')
    ordering = ('topic__order', 'order')

    OrderedModelAdmin.move_up_down_links.short_description = 'Зміна порядку'


admin.site.register(Article, ArticleAdmin)
admin.site.register(Topic, TopicAdmin)