from django.shortcuts import render
from django.views.generic.detail import DetailView

import markdown

from .utils.markdown_extensions import (
    mathjax,
    tables
)
from .models import Article


class ArticleView(DetailView):
    model = Article
    context_object_name = 'article'
    template_name = 'book/article.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rendered_article'] = markdown.markdown(
            self.object.content, 
            extensions=[
                mathjax.MathJaxExtension(),
                tables.TableExtension(),
            ]
        )

        return context

