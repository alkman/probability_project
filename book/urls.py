from django.urls import path

from .views import ArticleView

urlpatterns = [
    path('article/<int:pk>', ArticleView.as_view(), name='article')
]