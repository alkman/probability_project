from django.db import models
from django.utils.translation import gettext_lazy as _
from django.db.models import F, Max
from django.urls import reverse

from easy_thumbnails.fields import ThumbnailerImageField

from ordered_model.models import OrderedModel, OrderedModelBase


class Topic(OrderedModel):
    
    name = models.CharField(
        max_length=128,
        verbose_name=_('Назва теми')
    )

    order = models.PositiveIntegerField(
        editable=False, 
        db_index=True,
        verbose_name=_('Порядок тем')
    )

    def save(self, *args, **kwargs):
        if getattr(self, self.order_field_name) is None:
            c = self.get_ordering_queryset().aggregate(Max(self.order_field_name)).get(self.order_field_name + '__max')
            setattr(self, self.order_field_name, 1 if c is None else c + 1)
        super(OrderedModelBase, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Тема')
        verbose_name_plural = _('Теми')


class Article(OrderedModel):

    topic = models.ForeignKey(
        Topic,
        on_delete=models.SET_NULL,
        related_name='articles',
        blank=True,
        null=True,
        verbose_name=_('Тема')
    )

    name = models.CharField(
        max_length=128,
        verbose_name=_('Назва статті')
    )

    content = models.TextField(
        verbose_name=_('Контент')
    )

    order_with_respect_to = 'topic'
    order = models.PositiveIntegerField(
        editable=False, 
        db_index=True,
        verbose_name=_('Порядок статей')
    )

    pub_date = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_('Дата публікації')
    )

    change_date = models.DateTimeField(
        auto_now=True,
        verbose_name=_('Остання зміна')
    )

    def save(self, *args, **kwargs):
        if getattr(self, self.order_field_name) is None:
            c = self.get_ordering_queryset().aggregate(Max(self.order_field_name)).get(self.order_field_name + '__max')
            setattr(self, self.order_field_name, 1 if c is None else c + 1)
        super(OrderedModelBase, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('book:article', kwargs={'pk': self.id})

    class Meta(OrderedModel.Meta):
        verbose_name = _('Стаття')
        verbose_name_plural = _('Статті')


class ImageAtachment(models.Model):

    article = models.ForeignKey(
        Article,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_('Стаття')
    )

    image = ThumbnailerImageField(
        verbose_name=_('Фото')
    )

    def __str__(self):
        return self.image.name

    def get_absolute_url(self):
        return self.image.url



